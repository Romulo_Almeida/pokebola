

var request = require("request");
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
mongoose.connect('mongodb://mariza:dsa321ewq@mariza-shard-00-00-a1egm.mongodb.net:27017,mariza-shard-00-01-a1egm.mongodb.net:27017,mariza-shard-00-02-a1egm.mongodb.net:27017/pokebola?ssl=true&replicaSet=Mariza-shard-0&authSource=admin&retryWrites=true',{ useNewUrlParser: true });

mongoose.options = {
  debug: false
}
/*var options = {
  url: 'https://api.ipify.org/?format=json',
  proxy: 'http://177.107.169.110:3128'
 }
request(options, function (error, response, body) {
          console.log(body)
})
  return true;
*/


var cheerio = require('cheerio');

var Provas_Questoes = require("./models/Questoes_Provas");
var Imagem = require("./models/Imagens");
var listagem = [];
Provas_Questoes.find({$or: [
  {imagem_pre: {$ne:""}},
  {imagem_pos: {$ne:""}},
  {pergunta_pre: {$regex: /<img/}},
  {pergunta_pos: {$regex: /<img/}},
  {"opcoes.opcao": {$regex: /<img/}},// Falta colocar..
  {"opcoes.imagem" : {$ne:""}}      // Falta colocar..
]} ).lean().exec((err, questoes)=>{
  console.log("Carregado para a memória..")
  questoes.forEach(elem => {
    let busca = []; 
    busca = busca.concat(getImagesUrls(elem.imagem_pre));
    busca = busca.concat(getImagesUrls(elem.pergunta_pre));
    busca = busca.concat(getImagesUrls(elem.pergunta_pos));

    if (elem.imagem_pre != "")
      busca = busca.concat(getImagesUrls("<img src='"+ elem.imagem_pre +"' />"));
    
    if (elem.imagem_pos != "")
      busca = busca.concat(getImagesUrls("<img src='"+ elem.imagem_pos +"' />"));

    if (busca.length > 0 )
    {
      listagem.push(busca);
    }
});
var promisses = [];
var count = 1;
  listagem.forEach(img => {
      var p = new Promise(function(resolve, reject) {

      Imagem.update({imagens: {'absolute': img.absolute } }, {imagens: img}, {upsert:true,new: true,strict:false}, function(err, doc){
          if (err) return console.log("Error");
          console.log(count+" de " + listagem.length)
          resolve();
          count++
      });

      });

      promisses.push(p);

  });

Promise.all(promisses)
    .then(function()
    { 
      console.log("Finish him!!")
    })

});

function getImagesUrls(html)
{
  const { URL } = require('url');
  lista = [];
  if (html){
    try {
    $ = cheerio.load(html);
      var imgs = $("img");
      if (imgs){
        imgs.each(function(i, elem) {
          let src = $(elem).attr("src");
          var absolute = new URL(src, 'http://enem.estuda.com');
          var obj = {src: src, absolute: absolute.href }
          lista.push(obj);
        });
      }
    } catch (error) {
      console.log("Erro em: ")
      console.log(error)
    }
  }
  return lista;
}
